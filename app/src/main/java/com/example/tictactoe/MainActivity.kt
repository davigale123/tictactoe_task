package com.example.tictactoe

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var score1: TextView
    private lateinit var score2: TextView
    private lateinit var grid: Array<Array<Button>>

    private var activePlayer = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        score1 = findViewById(R.id.score1)
        score2 = findViewById(R.id.score2)
        val newRound: Button = findViewById(R.id.newRound)
        val endGame: Button = findViewById(R.id.Exit)
        val A0: Button = findViewById(R.id.A0)
        val A1: Button = findViewById(R.id.A1)
        val A2: Button = findViewById(R.id.A2)
        val A3: Button = findViewById(R.id.A3)
        val A4: Button = findViewById(R.id.A4)
        val A5: Button = findViewById(R.id.A5)
        val A6: Button = findViewById(R.id.A6)
        val A7: Button = findViewById(R.id.A7)
        val A8: Button = findViewById(R.id.A8)

        val lineA = arrayOf(A0, A1, A2)
        val lineB = arrayOf(A3, A4, A5)
        val lineC = arrayOf(A6, A7, A8)

        grid = arrayOf(lineA, lineB, lineC)

        A0.setOnClickListener(this)
        A1.setOnClickListener(this)
        A2.setOnClickListener(this)
        A3.setOnClickListener(this)
        A4.setOnClickListener(this)
        A5.setOnClickListener(this)
        A6.setOnClickListener(this)
        A7.setOnClickListener(this)
        A8.setOnClickListener(this)


        newRound.setOnClickListener {
            clearGame()
            activePlayer = 1
        }

        endGame.setOnClickListener {
            score1.text = "Player one: 0"
            score2.text = "Player two: 0"
            clearGame()
            activePlayer = 1
        }

    }

    override fun onClick(v: View?) {
        val btnClicked = findViewById<Button>(v!!.id)

        if (activePlayer == 1 && btnClicked.text.isEmpty()) {
            btnClicked.text = "X"
            btnClicked.setTextColor(Color.YELLOW)
            activePlayer = 2
            val result = winner("X")
            if (result) {
                score1.text = score1.text.substring(0, 12) + (Integer.parseInt(score1.text.substring(12)) + 1)
                Toast.makeText(this, "player one wins", Toast.LENGTH_SHORT).show()
                activePlayer = 0
            }
        } else if (activePlayer == 2 && btnClicked.text.isEmpty()) {
            btnClicked.text = "O"
            btnClicked.setTextColor(Color.RED)
            activePlayer = 1
            val result = winner("O")
            if (result) {
                score2.text = score2.text.substring(0, 12) + (Integer.parseInt(score2.text.substring(12)) + 1)
                Toast.makeText(this, "player two wins", Toast.LENGTH_SHORT).show()
                activePlayer = 0
            }
        }
    }

    private fun clearGame() {
        for (i in 0..2) {
            for (j in 0..2) {
                grid[i][j].text = ""
            }
        }
    }

    private fun winner(turn: String): Boolean {
        for (i in 0..2) {
            var isVerticalWin = true
            var isHorizontalWin = true
            for (j in 0..2) {
                if (grid[i][j].text.toString() != turn) {
                    isVerticalWin = false
                }
                if (grid[j][i].text.toString() != turn) {
                    isHorizontalWin = false
                }
            }
            if (isVerticalWin || isHorizontalWin) {
                return true
            }
        }

        if (grid[0][0].text == turn && grid[1][1].text == turn && grid[2][2].text == turn) {
            return true
        }
        if (grid[2][0].text == turn && grid[1][1].text == turn && grid[0][2].text == turn) {
            return true
        }

        return false
    }
}


